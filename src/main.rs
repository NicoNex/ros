#![no_std]
#![no_main]

mod vga_buffer;

use core::panic::PanicInfo;


#[no_mangle]
pub extern "C" fn _start() -> ! {
	vga_buffer::print_something("1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23\n24\n25\n26");
	vga_buffer::print_something("sas mike");
	
	loop {}
}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
	loop {}
}
